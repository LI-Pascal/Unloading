﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Actcut
{
    class DprHeader
    {
        private static string ResizeNullTerminatedStringNoTrim(StringBuilder str)
        {
            var result = (str.ToString()).TrimEnd('\0').TrimEnd();
            return result;
        }

        #region Dll calls
        [DllImport("DprHeader.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "DPRH_Read")]
        private static extern Int32 DPRH_Read_(string file);
        [DllImport("DprHeader.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "DPRH_Write")]
        private static extern Int32 DPRH_Write_(string file);
        [DllImport("DprHeader.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "DPRH_Update")]
        public static extern Int32 DPRH_Update_(string file);
        [DllImport("DprHeader.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "DPRH_GetVarName")]
        private static extern Int32 DPRH_GetVarName_(StringBuilder varName);
        [DllImport("DprHeader.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "DPRH_GetVarString")]
        private static extern Int32 DPRH_GetVarString_(string varName, StringBuilder value);
        [DllImport("DprHeader.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "DPRH_SetVarString")]
        private static extern Int32 DPRH_SetVarString_(string varName, string value);
        [DllImport("DprHeader.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "DPRH_GetVarValue")]
        private static extern Int32 DPRH_GetVarValue_(string varName, ref double value, ref Int32 nIN, ref Int32 nOut);
        [DllImport("DprHeader.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "DPRH_SetVarValue")]
        private static extern Int32 DPRH_SetVarValue_(string varName, ref double value, ref Int32 precis, ref Int32 nOut);
        [DllImport("DprHeader.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "DPRH_DeleteVar")]
        private static extern Int32 DPRH_DeleteVar_(string file);
        [DllImport("DprHeader.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "DPRHREADAUTORISATION")]
        private static extern Int32 DPRH_ReadAutorisation_(
            string filename, Int32 lenStr,
            ref Int32 sym0, ref Int32 symX, ref Int32 symY, ref Int32 symXY,
            ref Int32 rotauto, ref Int32 rotmanu, ref Int32 ecastan,
            ref Int32 rotats1, ref Int32 rotats2, ref Int32 rotats3,
            ref double ecagauche, ref double ecadroit, ref double ecabas, ref double ecahaut);
        [DllImport("DprHeader.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "DPRHWRITEAUTORISATION")]
        private static extern Int32 DPRH_WriteAutorisation_(
            string filename, Int32 lenStr,
            ref Int32 sym0, ref Int32 symX, ref Int32 symY, ref Int32 symXY,
            ref Int32 rotauto, ref Int32 rotmanu, ref Int32 ecastan,
            ref Int32 rotats1, ref Int32 rotats2, ref Int32 rotats3,
            ref double ecagauche, ref double ecadroit, ref double ecabas, ref double ecahaut);
        [DllImport("DprHeader.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "DPRHREADSHEET")]
        private static extern Int32 DPRH_ReadSheet_(string filename, Int32 lenStr, ref double length, ref double width);
        [DllImport("DprHeader.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "DPRHREADKERF")]
        private static extern Int32 DPRH_ReadKerf_(string filename, Int32 lenStr, ref double kerf);
        #endregion

        #region public calls
        public static int DPRH_GetVarName(out string varName)
        {
            StringBuilder s = new StringBuilder("", 512);
            int result = DPRH_GetVarName_(s);
            varName = ResizeNullTerminatedStringNoTrim(s); ;
            return result;
        }
        public static int DPRH_GetVarString(string varName, ref string value)
        {
            StringBuilder s = new StringBuilder("", 512);
            int result = DPRH_GetVarString_(varName, s);
            value = ResizeNullTerminatedStringNoTrim(s);
            return result;
        }
        public static int DPRH_SetVarString(string varName, string value)
        {
            return DPRH_SetVarString_(varName, value);
        }
        public static int DPRH_GetVarValue(string varName, ref double value, ref int nIn, ref int nOut)
        {
            int result = DPRH_GetVarValue_(varName, ref value, ref nIn, ref nOut);
            return result;
        }
        public static int DPRH_SetVarValue(string varName, ref double value, ref int precis, ref int nOut)
        {
            int result = DPRH_SetVarValue_(varName, ref value, ref precis, ref nOut);
            return result;
        }
        public static int DPRH_DeleteVar(string file)
        {
            return DPRH_DeleteVar_(file);
        }
        public static int DPRH_Read(string file)
        {
            return DPRH_Read_(file);
        }
        public static int DPRH_Write(string file)
        {
            return DPRH_Write_(file);
        }
        public static int DPRH_Update(string file)
        {
            return DPRH_Update_(file);
        }
        public int DPRH_ReadAutorisation(
            string filename, Int32 lenStr,
            ref Int32 sym0, ref Int32 symX, ref Int32 symY, ref Int32 symXY,
            ref Int32 rotauto, ref Int32 rotmanu, ref Int32 ecastan,
            ref Int32 rotats1, ref Int32 rotats2, ref Int32 rotats3,
            ref double ecagauche, ref double ecadroit, ref double ecabas, ref double ecahaut)
        {
            return DPRH_ReadAutorisation_(
            filename, lenStr,
            ref sym0, ref symX, ref symY, ref symXY,
            ref rotauto, ref rotmanu, ref ecastan,
            ref rotats1, ref rotats2, ref rotats3,
            ref ecagauche, ref ecadroit, ref ecabas, ref ecahaut);
        }
        public int DPRH_WriteAutorisation(
            string filename, Int32 lenStr,
            ref Int32 sym0, ref Int32 symX, ref Int32 symY, ref Int32 symXY,
            ref Int32 rotauto, ref Int32 rotmanu, ref Int32 ecastan,
            ref Int32 rotats1, ref Int32 rotats2, ref Int32 rotats3,
            ref double ecagauche, ref double ecadroit, ref double ecabas, ref double ecahaut)
        {
            return DPRH_WriteAutorisation_(
            filename, lenStr,
            ref sym0, ref symX, ref symY, ref symXY,
            ref rotauto, ref rotmanu, ref ecastan,
            ref rotats1, ref rotats2, ref rotats3,
            ref ecagauche, ref ecadroit, ref ecabas, ref ecahaut);
        }
        public int DPRH_ReadSheet(string filename, int lenStr, ref double length, ref double width)
        {
            return DPRH_ReadSheet_(filename, lenStr, ref length, ref width);
        }
        public int DPRH_ReadKerf(string filename, int lenStr, ref double kerf)
        {
            return DPRH_ReadKerf_(filename, lenStr, ref kerf);
        }
        #endregion
    }
}
