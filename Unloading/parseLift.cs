﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Actcut
{

    public class Nesting
    {
        public Nesting(string agi, string key, string material, double thickness, double density)
        {
            this.Agi = agi;
            this.Key = key;
            this.Material = material;
            this.Thickness = thickness;
            this.Density = density;
        }

        public string Agi { get; set; }
        public string Key { get; set; }
        public string Material { get; set; }
        public double Thickness { get; set; }
        public double Density { get; set; }
        public List<Tuple<string, PartUnload>> NestedPart { get; private set; }
        public void Add(List<Tuple<string, PartUnload>> nestedPart)
        {
            this.NestedPart = nestedPart;
        }
    }

    public class PartUnload
    {
        public PartUnload()
        {
        }

        public PartUnload(string dpr, string key = "", string idconf = "", double posx = 0, double posy = 0, double rotationLift = 0, string status = "", double xlift = 0, double ylift = 0, double RotationLift = 0, string idgroup = "0")
        {
            this.Dpr = dpr;
            this.Key = key;
            this.IdConf = idconf;
            this.PosX = posx;
            this.PosY = posy;
            this.Rotation = rotationLift;
            this.Status = status;
            this.XLift = xlift;
            this.YLift = ylift;
            this.RotationLift = rotationLift;
            this.IdGroup = idgroup;
        }

        public string Dpr { get; set; }
        public string Key { get; set; }
        public string IdConf { get; set; }
        public double PosX { get; set; }
        public double PosY { get; set; }
        public double Rotation { get; set; }
        public string Status { get; set; }
        public double XLift { get; set; }
        public double YLift { get; set; }
        public double RotationLift { get; set; }
        public string IdGroup { get; set; }
    }

    public class Part
    {
        public string dimx;
        public string dimy;
        public string idconf;
        public string x;
        public string y;
        public string rotation;
        public List<string> listsuctions;

        public Part(string dpr, string dimx, string dimy, string idconf, string x, string y, string rotation, List<string> listsuctions)
        {
            this.dimx = dimx;
            this.dimy = dimy;
            this.idconf = idconf;
            this.x = x;
            this.y = y;
            this.rotation = rotation;
            this.listsuctions = listsuctions;
        }
    }

}
