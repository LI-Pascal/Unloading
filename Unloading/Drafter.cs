using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace Actcut
{
    public class Drafter
    {
        public Drafter()
        {
            Drafter.AP_initDrafter(0);
        }

        #region Dll calls
        [DllImport("Drafter.dll", CallingConvention = CallingConvention.StdCall, EntryPoint = "AP_initDrafter")]
        private static extern void AP_initDrafter(Int32 apFile);
        [DllImport("Drafter.dll", CallingConvention = CallingConvention.StdCall, EntryPoint = "AP_readFile")]
        private static extern Int32 AP_readFile_(string aFile, Int32 fileType, out Int32 error);
        [DllImport("Drafter.dll", CallingConvention = CallingConvention.StdCall, EntryPoint = "AP_firstContour")]
        private static extern Int32 AP_firstContour_(Int32 aFile, out Int32 aContourType);
        [DllImport("Drafter.dll", CallingConvention = CallingConvention.StdCall, EntryPoint = "AP_nextContour")]
        private static extern Int32 AP_nextContour_(Int32 aFile, out Int32 aContourType);
        [DllImport("Drafter.dll", CallingConvention = CallingConvention.StdCall, EntryPoint = "AP_getFileType")]
        private static extern Int32 AP_getFileType_([MarshalAs(UnmanagedType.LPStr)] String suffixWithPoint);
        [DllImport("Drafter.dll", CallingConvention = CallingConvention.StdCall, EntryPoint = "AP_getContourDimensions")]
        private static extern Int32 AP_getContourDimensions_(Int32 aContour, out double xmin, out double ymin, out double xmax, out double ymax);
        [DllImport("Drafter.dll", CallingConvention = CallingConvention.StdCall, EntryPoint = "AP_firstElement")]
        private static extern Int32 AP_firstElement_(Int32 aContour, out Int32 aElementType);
        [DllImport("Drafter.dll", CallingConvention = CallingConvention.StdCall, EntryPoint = "AP_getLine")]
        private static extern Int32 AP_getLine_(Int32 aElement, out double x1, out double x2, out double y1, out double y2, out Int32 aTool);
        [DllImport("Drafter.dll", CallingConvention = CallingConvention.StdCall, EntryPoint = "AP_getArc")]
        private static extern Int32 AP_getArc_(Int32 aElement, out double x1, out double x2, out double y1, out double y2, out double xc, out double yc, out Int32 antiClockWise, out Int32 aTool);
        [DllImport("Drafter.dll", CallingConvention = CallingConvention.StdCall, EntryPoint = "AP_getElementDelardage")]
        private static extern Int32 AP_getElementDelardage_(Int32 aElement, out Int32 delType, out Int32 delEtat1, out Int32 delEtat2, out Int32 delEtat3, out Int32 delEtat4);
        [DllImport("Drafter.dll", CallingConvention = CallingConvention.StdCall, EntryPoint = "AP_getElementChamfer")]
        private static extern Int32 AP_getElementChamfer_(Int32 aElement, out Int32 chfType, out Int32 chfEtat1, out Int32 chfEtat2, out Int32 chfEtat3, out Int32 chfEtat4);
        [DllImport("Drafter.dll", CallingConvention = CallingConvention.StdCall, EntryPoint = "AP_nextElement")]
        private static extern Int32 AP_nextElement_(Int32 aContour, Int32 aElement, out Int32 aElementType);
        [DllImport("drafter.dll", CallingConvention = CallingConvention.StdCall, EntryPoint = "AP_close")]
        private static extern void AP_close_(long file);
        [DllImport("drafter.dll", CallingConvention = CallingConvention.StdCall, EntryPoint = "AP_doPalletPart")]
        private static extern Int32 AP_doPalletPart_(Int32 readFile, string aFile, string aNestKey, string aOutputDir, string aNamePrefix, out Int32 NbPart, out Int32 aError);
        [DllImport("drafter.dll", CallingConvention = CallingConvention.StdCall, EntryPoint = "AP_firstPalletPart")]
        private static extern Int32 AP_firstPalletPart_();
        [DllImport("drafter.dll", CallingConvention = CallingConvention.StdCall, EntryPoint = "AP_nextPalletPart")]
        private static extern Int32 AP_nextPalletPart_();
        [DllImport("drafter.dll", CallingConvention = CallingConvention.StdCall, EntryPoint = "AP_getPalletPartInfo")]
        private static extern void AP_getPalletPartInfo_(Int32 aPallet, string aName, Int32 maxLen, out Int32 qte, out double x, out double y, out double thick
            , out int evacType, out int noEmplac, out int p3, out int p4, out int p5, out int p6, out int p7, out int p8, out double dxPt, out double dyPt
            , out double yMove, out int trumaType, out int trumaAngle);
        [DllImport("drafter.dll", CallingConvention = CallingConvention.StdCall, EntryPoint = "AP_isVentouseActive")]
        private static extern void AP_isVentouseActive_(Int32 noVentouse, Int32 p3, Int32 p4, Int32 p5, Int32 p6, Int32 p7, Int32 p8, [MarshalAs(UnmanagedType.I1)] out bool isActive);
        #endregion

        #region public functions
        public Int32 AP_readFile(string aFile, Int32 fileType, out Int32 error)
        {
            return AP_readFile_(aFile, fileType, out error);
        }
        public Int32 AP_firstContour(Int32 aFile, out Int32 aContourType)
        {
            return AP_firstContour_(aFile, out aContourType);
        }
        public Int32 AP_nextContour(Int32 aFile, out Int32 aContourType)
        {
            return AP_nextContour_(aFile, out aContourType);
        }
        public Int32 AP_getFileType(string suffixWithPoint)
        {
            return AP_getFileType_(suffixWithPoint);
        }
        public Int32 AP_getContourDimensions(Int32 aContour, out double xmin, out double xmax, out double ymin, out double ymax)
        {
            return AP_getContourDimensions_(aContour, out xmin, out xmax, out ymin, out ymax);
        }
        public Int32 AP_firstElement(Int32 aContour, out Int32 aElementType)
        {
            return AP_firstElement_(aContour, out aElementType);
        }
        public Int32 AP_getLine(Int32 aElement, out double x1, out double x2, out double y1, out double y2, out Int32 aTool)
        {
            return AP_getLine_(aElement, out x1, out x2, out y1, out y2, out aTool);
        }
        public Int32 AP_getArc(Int32 aElement, out double x1, out double x2, out double y1, out double y2, out double xc, out double yc, out Int32 antiClockWise, out Int32 aTool)
        {
            return AP_getArc_(aElement, out x1, out x2, out y1, out y2, out xc, out yc, out antiClockWise, out aTool);
        }
        public Int32 AP_getElementDelardage(Int32 aElement, out Int32 delType, out Int32 delEtat1, out Int32 delEtat2, out Int32 delEtat3, out Int32 delEtat4)
        {
            return AP_getElementDelardage_(aElement, out delType, out delEtat1, out delEtat2, out delEtat3, out delEtat4);
        }
        public Int32 AP_getElementChamfer(Int32 aElement, out Int32 chfType, out Int32 chfEtat1, out Int32 chfEtat2, out Int32 chfEtat3, out Int32 chfEtat4)
        {
            return AP_getElementChamfer_(aElement, out chfType, out chfEtat1, out chfEtat2, out chfEtat3, out chfEtat4);
        }
        public Int32 AP_nextElement(Int32 aContour, Int32 aElement, out Int32 aElementType)
        {
            return AP_nextElement_(aContour, aElement, out aElementType);
        }
        public void AP_close(Int32 file)
        {
            AP_close_(file);
        }
        public void AP_doPalletPart(Int32 readFile, string aFile, string aNestKey, string aOutputDir, string aNamePrefix, out Int32 NbPart, out Int32 aError)
        {
            AP_doPalletPart_(readFile, aFile, aNestKey, aOutputDir, aNamePrefix, out NbPart, out aError);
        }
        public Int32 AP_firstPalletPart()
        {
            return AP_firstPalletPart_();
        }
        public Int32 AP_nextPalletPart()
        {
            return AP_nextPalletPart_();
        }
        public void AP_getPalletPartInfo(Int32 aPallet, string aName, Int32 maxLen, out Int32 qte, out double x, out double y, out double thick
            , out int evacType, out int noEmplac, out int p3, out int p4, out int p5, out int p6, out int p7, out int p8, out double dxPt, out double dyPt
            , out double yMove, out int trumaType, out int trumaAngle)
        {
            AP_getPalletPartInfo_(aPallet, aName, maxLen, out qte, out x, out y, out thick
                , out evacType, out noEmplac, out p3, out p4, out p5, out p6, out p7, out p8, out dxPt, out dyPt
                , out yMove, out trumaType, out trumaAngle);
        }
        public void AP_isVentouseActive(Int32 noVentouse, Int32 p3, Int32 p4, Int32 p5, Int32 p6, Int32 p7, Int32 p8, out bool isActive)
        {
            AP_isVentouseActive_(noVentouse, p3, p4, p5, p6, p7, p8, out isActive);
        }
    }
    #endregion
}
