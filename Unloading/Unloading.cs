using Agi34;

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Xml.Serialization;

using Xml2CSharp;

//  3.10.2.2    Separateur decimal avait changé
//              ParseXmlUnload grip != null but count ==0
//              GroupPart une variable devait être = null an cas de groupe

namespace Actcut
{
    class Unloading
    {
        static readonly string logFolder = DefineAph.GetKeyPath("log");
        static readonly LogFile logFile = new LogFile(logFolder + "Log_MireResult.txt");
        static string decimaSeparator = Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator;

        static AgiRin aagi = new AgiRin();

        const string quote = "\"";
        static bool debug = false;
        static List<Tuple<string, Nesting>> nesting_list = new List<Tuple<string, Nesting>>();
        static List<Tuple<string, Part>> part_list = new List<Tuple<string, Part>>();
        static Int32 AP_File = 0;
        const double epsilon = 0.01;
        enum ActcutFileResult
        {
            FILE_ERROR = -1,
            FILE_OK = 0
        }

        enum ActcutElementResult
        {
            AP_NO_CONTOUR = -1,
            AP_NO_ELEMENT = -1
        }

        [STAThread]
        static void Main(string[] args)
        {
            var executablePath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            Configuration config = ConfigurationManager.OpenExeConfiguration(executablePath);
            Console.Clear();
            if (args.Length != 2)
            {
                Console.WriteLine("Unloading 3.10.2");
                Console.WriteLine("Unloading.exe AgiFullPath KeyNestNumber");
                Console.WriteLine("Unloading.exe \"c:\\alma\\data\\Laser\\Rin\\123.agi\" \"KEY_NEST_1\"");
                Console.WriteLine("Appuyez une touche pour continuer . . .");
                Console.ReadKey(true);
                return;
            }

            List<string> errorCfgFile = new List<string>();

            if (!File.Exists(args[0]))
            {
                errorCfgFile.Add("Unloading.exe \"" + args[0] + "\" \"" + args[1] + "\"");
                errorCfgFile.Add(args[0] + " fichier introuvable");
                ViewLog(errorCfgFile);
                return;
            }

            //  args must be 
            //      agiFileName NestKey ("C:\alma\Actcut39\Alma_NTX\imb\agi\512.agi" "KEY_NEST_1")
            if (args.Count() != 2)
                return;

            string agiFileName = args[0];
            string nestKey = args[1];
            #region save settings
            config = ConfigurationManager.OpenExeConfiguration(executablePath);
            if (config.AppSettings.Settings["agiFileName"] != null)
                config.AppSettings.Settings["agiFileName"].Value = agiFileName;
            else
                config.AppSettings.Settings.Add("agiFileName", agiFileName);

            if (config.AppSettings.Settings["nestKey"] != null)
                config.AppSettings.Settings["nestKey"].Value = nestKey;
            else
                config.AppSettings.Settings.Add("nestKey", nestKey);

            config.Save(ConfigurationSaveMode.Full, true);
            #endregion

            Paletisation(agiFileName, nestKey);
        }
        private static void Paletisation(string agiFileName, string nestKey = "")
        {
            string dirName = Path.GetDirectoryName(agiFileName);
            string fileName = Path.GetFileName(agiFileName);
            string aname = Path.GetFileNameWithoutExtension(agiFileName);
            string ext = Path.GetExtension(agiFileName);
            string sepa = string.Concat(quote, " ", quote);
            string arg = string.Empty;
            string destSvg = string.Empty;
            string outputfile = string.Empty;
            string cmd = string.Empty;
            int idgroup = 0;
            //Drafter.AP_initDrafter(0);
            if (!string.IsNullOrEmpty(nestKey))
                arg = "NESTING|" + agiFileName + "|" + nestKey;
            else
                arg = "RIN|" + agiFileName;

            if (!string.IsNullOrEmpty(DefineAph.GetKeyPath("svg")))
                destSvg = DefineAph.GetKeyPath("svg");
            else
                destSvg = DefineAph.GetKeyPath("rin");

            string offLineUnloadingMgrPath = string.Concat(quote, DefineAph.GetBinPath(), "OfflineUnloadingMgr.exe", quote);
            outputfile = Path.Combine(destSvg, fileName + ".xml");

            Console.WriteLine("Unloading 3.10.2");
            Thread.Sleep(1000);

            cmd = string.Concat(new string[]
            { quote, DefineAph.GetRscPath(),
                "OLUMachine-STX.xml", sepa, arg, sepa, outputfile, quote });
            ProcessStartInfo startInfo = new ProcessStartInfo
            {
                FileName = offLineUnloadingMgrPath,
                Arguments = cmd,
                UseShellExecute = false,
                RedirectStandardOutput = true,
                CreateNoWindow = true
            };

            if (!debug)
            {
                Process process = new Process
                {
                    StartInfo = startInfo
                };
                Console.WriteLine("Computing Result");
                Console.WriteLine(startInfo.FileName);
                Console.WriteLine(startInfo.Arguments);
                process.Start();
                while (!process.StandardOutput.EndOfStream)
                {
                    string line = process.StandardOutput.ReadLine();
                    Console.WriteLine(line);
                }
            }
            else
                Clipboard.SetText(cmd);

            ParseXmlUnload(outputfile);

            PartUnload aliftpart;
            string file = Path.Combine(dirName, fileName);
            List<string> tabNest = new List<string>();

            Drafter drafter = new Drafter();

            if (File.Exists(file))
            {
                int nbp = 0;

                aagi.ReadAgiFile(file, false, "Export");
                var agiParts = aagi.Parts;
                var agiSheets = aagi.Formats;
                var agithickness = aagi.Material.thickness;
                if (string.IsNullOrEmpty(nestKey))
                    foreach (AgiNesting agiNest in aagi.Nestings)
                        tabNest.Add(agiNest.Key);
                else
                    tabNest.Add(nestKey);
                {
                    string DimX = "";
                    string DimY = "";
                    foreach (string NestKey in tabNest)
                    {
                        AgiNesting agiNesting = aagi.Nestings[NestKey];
                        var aliftnest = nesting_list.Where(p => p.Item1 == agiNesting.Key).FirstOrDefault();
                        var formatDimX = agiNesting.Format.DimX;
                        var formatDimY = agiNesting.Format.DimY;
                        int partswithsolution = 0;
                        List<string> outArray = new List<string>();
                        foreach (AgiPosition npart in agiNesting.Positions)
                        {
                            AgiPart apart = npart.part;
                            string NameDpr = Path.Combine(DefineAph.GetContextPath(), apart.FileName);
                            Console.WriteLine("processing " + NameDpr);
                            string id_left = apart.GetFieldValue("ID_LEFT");
                            string nav_pan = id_left.Split('-')[0] + "-";

                            string dest = apart.GetFieldValue("fullwplace");
                            if (string.IsNullOrEmpty(dest))
                                dest = apart.GetFieldValue("wplace");

                            var manualunload = 0;
                            string poste = apart.GetFieldValue("wchain");
                            if (poste == "27")
                                manualunload = 1;

                            string index = ((string)apart.GetFieldValue("idp")).Split('-').Last();
                            var npid = nav_pan + "-" + index + "-" + dest;
                            var size = apart.GetFieldValue("size");

                            DprHeader.DPRH_Read(NameDpr);
                            string dimens = "";
                            DprHeader.DPRH_GetVarString("DIMENS", ref dimens);
                            DimX = dimens.Split(' ')[0];
                            DimY = dimens.Split(' ')[0];
                            double surface = 0;
                            int valIn = 1;
                            int valOut = 1;
                            DprHeader.DPRH_GetVarValue("SURFACE", ref surface, ref valIn, ref valOut);
                            if (size != "B")
                            {
                                logFile.WriteLine("Part Too big  " + NameDpr + Environment.NewLine);
                                continue;
                            }
                            aliftpart = aliftnest.Item2.NestedPart.Where(p => p.Item1 == npart.Key).First().Item2;
                            double[] res = GetChanfer(NameDpr);
                            var path = Path.GetDirectoryName(NameDpr);
                            var adprname = Path.GetFileName(NameDpr);
                            if (res != null && res.Length == 1)
                                res = GetChanfer(Path.Combine(path, "ori\\" + adprname));
                            nbp++;
                            idgroup = int.Parse(aliftpart.IdGroup);
                            if (idgroup > 0)
                                res = null;
                            double XPart = npart.PosX;
                            double YPart = npart.PosY;
                            double AngPart = npart.Angle;
                            double weight = 7.85 * aagi.Material.thickness * surface / (1000000);
                            int aErr = 0;
                            int nbPart = 0;
                            double Ang = AngPart;
                            double ang = NormalisationAngle(npart.Angle, npart.SymX, npart.SymY);
                            string Chamfer = "";
                            double X = 0, Y = 0, Angtruma = 0;
                            if (res != null)
                            {
                                double x1t = res[0];
                                double y1t = res[1];
                                double x2t = res[2];
                                double y2t = res[3];
                                double x3t = res[4];
                                double y3t = res[5];
                                double x4t = res[6];
                                double y4t = res[7];
                                double x1 = XPart + x1t * MyCos(ang) - y1t * MySin(ang);
                                double y1 = YPart + x1t * MySin(ang) + y1t * MyCos(ang);

                                double x2 = XPart + x2t * MyCos(ang) - y2t * MySin(ang);
                                double y2 = YPart + x2t * MySin(ang) + y2t * MyCos(ang);

                                double x3 = XPart + x3t * MyCos(ang) - y3t * MySin(ang);
                                double y3 = YPart + x3t * MySin(ang) + y3t * MyCos(ang);

                                double x4 = XPart + x4t * MyCos(ang) - y4t * MySin(ang);
                                double y4 = YPart + x4t * MySin(ang) + y4t * MyCos(ang);
                                Chamfer = StrRound(x1) + ";" + StrRound(y1) + ";" + StrRound(x2) + ";" + StrRound(y2) + ";"
                                        + StrRound(x3) + ";" + StrRound(y3) + ";" + StrRound(x4) + ";" + StrRound(y4) + ";";
                            }
                            logFile.WriteLine("Status: " + aliftpart.Status + " For the part " + NameDpr + "\n");

                            string Ventouse = new string('0', 40);
                            if (aliftpart.Status == "Ok")
                            {
                                partswithsolution = 1;
                                X = aliftpart.XLift;
                                Y = aliftpart.YLift;
                                Angtruma = aliftpart.RotationLift;
                                Tuple<string, Part> axmlpart = part_list[int.Parse(aliftpart.IdConf)];
                                List<string> sucs = axmlpart.Item2.listsuctions;

                                if (manualunload != 1)
                                {
                                    Ventouse = "";
                                    for (int counter = 1; counter <= 40; counter++)
                                        if (sucs.Contains(counter.ToString()))
                                            Ventouse += "1";
                                        else
                                            Ventouse += "0";
                                }
                            }
                            else if (aliftpart.Status == "NoSolutionFound" | aliftpart.Status == "NotToUnload")
                                Ventouse = new string('0', 40);
                            else if (aliftpart.Status == "LiftIntheDpr")
                            {
                                Console.WriteLine("lift in the dpr " + NameDpr);
                                apart = npart.part;
                                drafter.AP_doPalletPart(0, NameDpr, "", DefineAph.GetKeyPath("log"), "Test", out nbPart, out aErr);
                                int evacType = 0, noEmplac = 0, trumatype = 0, trumaangle = 0, qte = 0;
                                int p3 = 0, p4 = 0, p5 = 0, p6 = 0, p7 = 0, p8 = 0;
                                int Num_prehenseur = -1;
                                double thick = 0, dxPt = 0, dyPt = 0, yMove = 0;
                                if (aErr == 0)
                                {
                                    Int32 aPallet = drafter.AP_firstPalletPart();
                                    while (aPallet != -1)
                                    {
                                        aPallet = drafter.AP_firstPalletPart();
                                        drafter.AP_getPalletPartInfo(aPallet, aname, 256, out qte, out X, out Y, out thick, out evacType, out noEmplac
                                            , out p3, out p4, out p5, out p6, out p7, out p8, out dxPt, out dyPt, out yMove, out trumatype, out trumaangle);
                                        Ventouse = "";
                                        bool isActive = false;
                                        for (int noVentouse = 1; noVentouse <= 40; noVentouse++)
                                        {
                                            drafter.AP_isVentouseActive(noVentouse, p3, p4, p5, p6, p7, p8, out isActive);
                                            if (isActive)
                                                Ventouse += "1";
                                            else
                                                Ventouse += "0";
                                        }
                                        aPallet = drafter.AP_nextPalletPart();
                                    }
                                }
                                drafter.AP_close(AP_File);
                                if (evacType == 50)
                                    Num_prehenseur = 1;
                                else
                                {
                                    Num_prehenseur = 0;
                                    X = 0; Y = 0; Angtruma = 0;
                                    Ventouse = ""; Chamfer = "";
                                }
                                if (Num_prehenseur == 1)
                                {
                                    double posLiftX = X + dxPt;
                                    double posLiftY = Y + dyPt;
                                    // position lift
                                    X = XPart + posLiftX * MyCos(ang) - posLiftY * MySin(ang);
                                    Y = YPart + posLiftX * MySin(ang) + posLiftY * MyCos(ang);
                                    Angtruma = Ang + trumaangle;
                                    if (Math.Abs(Ang - 360) < epsilon)
                                        Ang = 0;
                                }
                            }
                            path = Path.GetDirectoryName(NameDpr);
                            adprname = Path.GetFileName(NameDpr);
                            aname = Path.GetFileName(NameDpr);

                            if (idgroup >= 0)
                                outArray.Add(aname + ";" + idgroup.ToString() + ";" + npid + ";" + StrRound(weight) + ";" + DimX + ";" + DimY + ";" + StrRound(X) + ";" + StrRound(Y) + ";" + StrRound(Angtruma) + ";" + Ventouse + ";" + Chamfer);
                        }
                        if (!File.Exists(DefineAph.GetKeyPath("LOG") + "MireResult.txt"))
                        {
                            logFile.WriteLine("Missing file mireresult.txt");
                            throw new NotImplementedException("Fichier MireResult.txt introuvable");
                        }

                        List<string> aginame = null;
                        try
                        {
                            aginame = File.ReadAllLines(DefineAph.GetKeyPath("LOG") + "MireResult.txt").ToList();
                        }
                        catch
                        {
                            Console.WriteLine("Erreur aginame");
                            logFile.WriteLine("Mire resultats manquants pour " + agiFileName);
                            throw new NotImplementedException("Mire resultats manquants pour " + agiFileName);
                        }
                        if (aginame.Count != 4)
                        {
                            Console.WriteLine("Erreur aginame");
                            logFile.WriteLine("Mire resultats manquants pour " + agiFileName);
                            throw new NotImplementedException("Mire resultats manquants pour " + agiFileName);
                        }
                        string filename = aginame[0];
                        nestKey = aginame[1];
                        string m1 = aginame[2];
                        string m2 = aginame[3];
                        string mire1 = m1.Split(' ')[0] + ";" + m1.Split(' ')[1];
                        string mire2 = m2.Split(' ')[0] + ";" + m2.Split(' ')[1];
                        string agamme = ((int)agiNesting.NestInfos.GetFieldValue("Gamme")).ToString();
                        string outfname = (DefineAph.GetKeyPath("Pallet")) + agamme;
                        string palName = "";

                        var desti = Path.Combine(destSvg, "Unloading Images").TrimEnd('\\') + "\\";
                        var destp = Path.Combine(destSvg, agamme).TrimEnd('\\') + "\\";
                        if (Directory.Exists(destp))
                            Directory.Delete(destp, true);

                        if (partswithsolution == 0)
                            palName = outfname + ".nopal";
                        else
                            palName = outfname + ".pal";
                        using (StreamWriter sw = new StreamWriter(palName))
                        {
                            sw.WriteLine("HEADER");
                            sw.WriteLine(StrRound(DimX) + '*' + StrRound(DimY) + '*' + StrRound(agithickness));
                            sw.WriteLine(outArray.Count().ToString());
                            sw.WriteLine(mire1);
                            sw.WriteLine(mire2);
                            sw.WriteLine("/HEADER");
                            sw.WriteLine("PARTS");
                            foreach (var lineToWrite in outArray)
                                sw.WriteLine(lineToWrite);
                            sw.WriteLine("/PARTS");
                        }
                        Directory.Move(desti, destp);
                    }
                }
            }
        }
        private static double NormalisationAngle(double angle, bool symx, bool symy)
        {
            double result = 0;
            if (symx & symy)
                result = angle + 180;
            else
                result = angle;

            if (result > 180)
                result -= 360;

            if (Math.Abs(result - 360) < epsilon)
                result = 0;

            return result;
        }

        private static double MyCos(double angle)
        {
            return Math.Cos(angle * Math.PI / 180);
        }
        private static string StrRound(double value, int digits = 3)
        {
            return Math.Round(value, digits).ToString();
        }
        private static string StrRound(string value, int digits = 3)
        {
            return Math.Round(MyCdbl(value), digits).ToString();
        }
        private static double MyCdbl(string value)
        {
            value = value.Replace(".", decimaSeparator).Replace(",", decimaSeparator);
            double.TryParse(value, out double result);
            return result;
        }
        private static double MySin(double angle)
        {
            return Math.Sin(angle * Math.PI / 180);
        }
        private static string GetValue(List<XAttribute> attributes, string key)
        {
            string result = null;
            try
            {
                result = attributes.Where(p => string.Equals(p.Name.ToString(), key, StringComparison.InvariantCultureIgnoreCase)).First().Value;
            }
            catch
            {
            }
            return result;
        }
        private static string GetValue(List<XElement> Xnodes, string key)
        {
            string result = null;
            try
            {
                result = Xnodes.Where(p => ((XElement)p).Name == key).First().Value;
            }
            catch
            {
            }
            return result;
        }
        private static void ParseXmlUnload(string filename)
        {
            Nesting anesting = null;
            XmlSerializer serializer = new XmlSerializer(typeof(UnloadingXml));
            UnloadingXml unloading;
            using (StringReader reader = new StringReader(File.ReadAllText(filename)))
            {
                unloading = (UnloadingXml)serializer.Deserialize(reader);
            }

            List<Tuple<string, PartUnload>> unloadList = new List<Tuple<string, PartUnload>>();

            foreach (var tt in unloading.Nestings_list.Nesting)
            {
                var agi = tt.Agi;
                var key = tt.Key;
                var material = tt.Material;
                var thickness = tt.Thickness;
                var density = tt.Density;
                anesting = new Nesting(agi, key, material, MyCdbl(thickness), MyCdbl(density));
                foreach (var nestedpart in tt.Nested_part)
                {
                    var dprName = nestedpart.Dpr;
                    var partKey = nestedpart.Position_key;
                    string status = nestedpart.Gripper_position.Status;
                    if (status == "Ok")
                    {
                        if (nestedpart.Group != null)
                        {
                            string groupID = nestedpart.Group.Id;
                            var PosX = MyCdbl(nestedpart.Gripper_position.X);
                            var PosY = MyCdbl(nestedpart.Gripper_position.Y);
                            var Rot = MyCdbl(nestedpart.Gripper_position.Rotation);
                            var Status = nestedpart.Gripper_position.Status;
                            foreach (var position_ in nestedpart.Group.Position)
                            {
                                PartUnload partUnload = new PartUnload
                                {
                                    Dpr = dprName,
                                    Key = partKey,
                                    IdConf = nestedpart.Configuration_id,
                                    PosX = 0,
                                    PosY = 0,
                                    Rotation = 0,
                                    Status = nestedpart.Gripper_position.Status,
                                    XLift = MyCdbl(nestedpart.Gripper_position.X),
                                    YLift = MyCdbl(nestedpart.Gripper_position.Y),
                                    RotationLift = MyCdbl(nestedpart.Gripper_position.Rotation),
                                    IdGroup = nestedpart.Group.Id
                                };
                                unloadList.Add(new Tuple<string, PartUnload>(position_.Key, partUnload));
                            }
                        }
                        else
                        {
                            PartUnload partUnload = new PartUnload
                            {
                                Dpr = dprName,
                                Key = partKey,
                                IdConf = nestedpart.Configuration_id,
                                PosX = MyCdbl(nestedpart.Part_position.X),
                                PosY = MyCdbl(nestedpart.Part_position.Y),
                                Rotation = MyCdbl(nestedpart.Part_position.Rotation),
                                Status = nestedpart.Gripper_position.Status,
                                XLift = MyCdbl(nestedpart.Gripper_position.X),
                                YLift = MyCdbl(nestedpart.Gripper_position.Y),
                                RotationLift = MyCdbl(nestedpart.Gripper_position.Rotation),
                                IdGroup = "0"
                            };
                            unloadList.Add(new Tuple<string, PartUnload>(partKey, partUnload));
                        }
                    }
                }
                anesting.Add(unloadList);
                nesting_list.Add(new Tuple<string, Nesting>(anesting.Key, anesting));
            }

            foreach (var tt in unloading.Parts_list.Part)
            {
                List<string> listsuctions = new List<string>();
                string x_ = "0";
                string y_ = "0";
                string rotation_ = "0";
                if (tt.Status == null)
                {
                    foreach (var ttt in tt.Part_configuration.Gripper.Activations.Sucker)
                        listsuctions.Add(ttt.Id);
                    x_ = tt.Part_configuration.Gripper.Relative_position.X;
                    y_ = tt.Part_configuration.Gripper.Relative_position.Y;
                    rotation_ = tt.Part_configuration.Gripper.Relative_position.Rotation;
                }

                Part apart = new Part(tt.Dpr, tt.Bounding_box.Width, tt.Bounding_box.Height, "1", x_, y_, rotation_, listsuctions);
                part_list.Add(new Tuple<string, Part>(tt.Part_configuration.Id, apart));

            }
        }
        private static double[] GetChanfer(string dprName)
        {
            var nbl = File.ReadAllLines(dprName).ToList();
            string aFile = Path.Combine(DefineAph.GetKeyPath("log"), "out.dpr");
            bool gid = false;
            double[] listp = null;
            int readori = 0;
            for (int i = 0; i < nbl.Count(); i++)
            {
                string l = nbl[i];
                if (gid)
                {
                    if (l.StartsWith("$ID"))
                    {
                        l = l.Replace("\n", "");
                        var tmp = l.Split(' ');
                        tmp[17] = "0";
                        nbl[i] = string.Join(" ", tmp);
                    }
                }

                if (l.StartsWith("*CTR"))
                    gid = true;
            }
            using (StreamWriter sw = new StreamWriter(aFile))
            {
                foreach (string line in nbl)
                    sw.WriteLine(line);
            }

            Int32 aContourType = 1;
            Int32 aContour = 1;
            Int32 aError = 1;
            string suffix = Path.GetExtension(aFile);

            Drafter drafter = new Drafter();
            AP_File = drafter.AP_readFile(aFile, drafter.AP_getFileType(suffix), out aError);

            aContour = drafter.AP_firstContour(AP_File, out aContourType);
            int nbchfVplus = 0;
            while (aContour != -1)
            {
                var chfType = 0;
                var index = 0;
                var frombev = false;
                if (aContourType == 2)
                {
                    double xmin = 0, xmax = 0, ymin = 0, ymax = 0;
                    Int32 aElementType = 0;

                    _ = drafter.AP_getContourDimensions(aContour, out xmin, out xmax, out ymin, out ymax);
                    Int32 aElm = drafter.AP_firstElement(aContour, out aElementType);
                    int nbelt = 0;
                    List<double[]> listelt = new List<double[]>();
                    while (aElm != -1)
                    {
                        double x1 = 0, x2 = 0, y1 = 0, y2 = 0, xc = 0, yc = 0;
                        Int32 delType = 0, delEtat1 = 0, delEtat2 = 0, delEtat3 = 0, delEtat4 = 0;
                        Int32 aElement = 0, aTool = 0, antiClockWise = 0;

                        chfType = 0;
                        nbelt++;
                        if (aElementType == 0)
                            aElement = drafter.AP_getLine(aElm, out x1, out y1, out x2, out y2, out aTool);
                        else if (aElementType == 1)
                            aElement = drafter.AP_getArc(aElm, out x1, out y1, out x2, out y2, out xc, out yc, out antiClockWise, out aTool);
                        else if (aElementType == 8)
                            ;   // point: not used
                        else if (aElementType == 9)
                            ;   // text: not used
                        if (aElementType == 0 | aElementType == 1)
                        {
                            listelt.Add(new double[] { x1, y1, x2, y2 });
                            aElement = drafter.AP_getElementDelardage(aElm, out delType, out delEtat1, out delEtat2, out delEtat3, out delEtat4);
                            if (delType != 3)   //  real V bevel
                            {
                                chfType = 0;
                                Int32 chfEtat1 = 0, chfEtat2 = 0, chfEtat3 = 0, chfEtat4 = 0;

                                aElement = drafter.AP_getElementChamfer(aElm, out chfType, out chfEtat1, out chfEtat2, out chfEtat3, out chfEtat4);
                                if (chfType != 0)
                                {
                                    if (chfType == 1)
                                    {
                                        nbchfVplus++;
                                        frombev = true;
                                        index = nbelt;
                                    }
                                }
                            }
                        }
                        aElm = drafter.AP_nextElement(aContour, aElm, out aElementType);
                    }
                    if (frombev)
                    {
                        if (nbelt == 1)
                            readori = 1;
                        else
                        {
                            double x1_ = 0, x2_ = 0, x3_ = 0, x4_ = 0, y1_ = 0, y2_ = 0, y3_ = 0, y4_ = 0;
                            if (index == nbelt)
                            {
                                x4_ = listelt[0][2];
                                y4_ = listelt[0][3];
                                x1_ = listelt[index - 2][0];
                                y1_ = listelt[index - 2][1];
                            }
                            else if (index == 1)
                            {
                                x1_ = listelt[nbelt - 1][0];
                                y1_ = listelt[nbelt - 1][1];
                                x4_ = listelt[index][0];
                                y4_ = listelt[index][1];
                            }
                            else
                            {
                                x1_ = listelt[index - 2][0];
                                y1_ = listelt[index - 2][1];
                                x4_ = listelt[index][2];
                                y4_ = listelt[index][3];
                            }
                            if (nbchfVplus == 1)
                            {
                                x2_ = listelt[index - 1][0];
                                y2_ = listelt[index - 1][1];
                                x3_ = listelt[index - 1][2];
                                y3_ = listelt[index - 1][3];
                            }
                            listp = new double[] { x1_, y1_, x2_, y2_, x3_, y3_, x4_, y4_ };
                        }
                        frombev = false;
                    }
                }
                aContour = drafter.AP_nextContour(AP_File, out aContourType);
            }
            drafter.AP_close(AP_File);
            if (nbchfVplus > 1)  //pas de traitement si plus d'un v plus
                return null;
            else if (readori == 1)
                return new double[] { -1 };
            else if (listp != null && listp.Length > 0)
                return listp;
            else
                return null;
        }
        private static void ViewLog(List<string> errorCfgFile)
        {
            string tmpFile = Path.GetTempFileName();
            using (StreamWriter sw = new StreamWriter(tmpFile))
            {
                foreach (string line in errorCfgFile)
                {
                    sw.WriteLine(line);
                }
            }
            Process.Start("notepad.exe", tmpFile);
        }
    }
}
