using System;
using System.Xml.Serialization;
using System.Collections.Generic;
namespace Xml2CSharp
{
    [XmlRoot(ElementName = "part_position")]
    public class Part_position
    {
        [XmlElement(ElementName = "x")]
        public string X { get; set; }
        [XmlElement(ElementName = "y")]
        public string Y { get; set; }
        [XmlElement(ElementName = "rotation")]
        public string Rotation { get; set; }
        [XmlElement(ElementName = "xflipped")]
        public string Xflipped { get; set; }
    }

    [XmlRoot(ElementName = "gripper_position")]
    public class Gripper_position
    {
        [XmlElement(ElementName = "x")]
        public string X { get; set; }
        [XmlElement(ElementName = "y")]
        public string Y { get; set; }
        [XmlElement(ElementName = "rotation")]
        public string Rotation { get; set; }
        [XmlAttribute(AttributeName = "status")]
        public string Status { get; set; }
    }

    [XmlRoot(ElementName = "nested_part")]
    public class Nested_part
    {
        [XmlElement(ElementName = "part_position")]
        public Part_position Part_position { get; set; }
        [XmlElement(ElementName = "gripper_position")]
        public Gripper_position Gripper_position { get; set; }
        [XmlAttribute(AttributeName = "dpr")]
        public string Dpr { get; set; }
        [XmlAttribute(AttributeName = "position_key")]
        public string Position_key { get; set; }
        [XmlAttribute(AttributeName = "configuration_id")]
        public string Configuration_id { get; set; }
        [XmlElement(ElementName = "group")]
        public Group Group { get; set; }
    }

    [XmlRoot(ElementName = "position")]
    public class Position
    {
        [XmlAttribute(AttributeName = "key")]
        public string Key { get; set; }
    }

    [XmlRoot(ElementName = "group")]
    public class Group
    {
        [XmlElement(ElementName = "position")]
        public List<Position> Position { get; set; }
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
    }

    [XmlRoot(ElementName = "nesting")]
    public class Nesting
    {
        [XmlElement(ElementName = "nested_part")]
        public List<Nested_part> Nested_part { get; set; }
        [XmlAttribute(AttributeName = "agi")]
        public string Agi { get; set; }
        [XmlAttribute(AttributeName = "key")]
        public string Key { get; set; }
        [XmlAttribute(AttributeName = "material")]
        public string Material { get; set; }
        [XmlAttribute(AttributeName = "thickness")]
        public string Thickness { get; set; }
        [XmlAttribute(AttributeName = "density")]
        public string Density { get; set; }
        [XmlAttribute(AttributeName = "display")]
        public string Display { get; set; }
    }

    [XmlRoot(ElementName = "nestings_list")]
    public class Nestings_list
    {
        [XmlElement(ElementName = "nesting")]
        public List<Nesting> Nesting { get; set; }
    }

    [XmlRoot(ElementName = "bounding_box")]
    public class Bounding_box
    {
        [XmlElement(ElementName = "width")]
        public string Width { get; set; }
        [XmlElement(ElementName = "height")]
        public string Height { get; set; }
    }

    [XmlRoot(ElementName = "relative_position")]
    public class Relative_position
    {
        [XmlElement(ElementName = "x")]
        public string X { get; set; }
        [XmlElement(ElementName = "y")]
        public string Y { get; set; }
        [XmlElement(ElementName = "rotation")]
        public string Rotation { get; set; }
    }

    [XmlRoot(ElementName = "sucker")]
    public class Sucker
    {
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "activations")]
    public class Activations
    {
        [XmlElement(ElementName = "sucker")]
        public List<Sucker> Sucker { get; set; }
    }

    [XmlRoot(ElementName = "gripper")]
    public class Gripper
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
        [XmlElement(ElementName = "relative_position")]
        public Relative_position Relative_position { get; set; }
        [XmlElement(ElementName = "activations")]
        public Activations Activations { get; set; }
        [XmlAttribute(AttributeName = "status")]
        public string Status { get; set; }
    }

    [XmlRoot(ElementName = "part_configuration")]
    public class Part_configuration
    {
        [XmlElement(ElementName = "id")]
        public string Id { get; set; }
        [XmlElement(ElementName = "xflipped")]
        public string Xflipped { get; set; }
        [XmlElement(ElementName = "display")]
        public string Display { get; set; }
        [XmlElement(ElementName = "gripper")]
        public Gripper Gripper { get; set; }
    }

    [XmlRoot(ElementName = "part")]
    public class Part
    {
        [XmlElement(ElementName = "area")]
        public string Area { get; set; }
        [XmlElement(ElementName = "bounding_box")]
        public Bounding_box Bounding_box { get; set; }
        [XmlElement(ElementName = "part_configuration")]
        public Part_configuration Part_configuration { get; set; }
        [XmlAttribute(AttributeName = "dpr")]
        public string Dpr { get; set; }
        [XmlAttribute(AttributeName = "status")]
        public string Status { get; set; }
        [XmlAttribute(AttributeName = "group_id")]
        public string Group_id { get; set; }
    }

    [XmlRoot(ElementName = "parts_list")]
    public class Parts_list
    {
        [XmlElement(ElementName = "part")]
        public List<Part> Part { get; set; }
    }

    [XmlRoot(ElementName = "unloading")]
    public class UnloadingXml
    {
        [XmlElement(ElementName = "nestings_list")]
        public Nestings_list Nestings_list { get; set; }
        [XmlElement(ElementName = "parts_list")]
        public Parts_list Parts_list { get; set; }
    }

}
