﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Actcut
{
    class LogFile
    {
        private string Name;
        public string name
        {
            get
            {
                return Name;
            }
            set
            {
                name = value;
            }
        }


        public LogFile(string logFileName)
        {
            this.Name = logFileName;
            if (File.Exists(logFileName))
                File.Delete(logFileName);

//            File.CreateText(logFileName);
        }

        public void WriteLine(string line)
        {
            File.AppendAllText(Name, line);
        }
    }
}
